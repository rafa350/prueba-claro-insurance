<?php

namespace Database\Seeders;

use App\Enums\Locations\LocationEnums;
use App\Models\State;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class StatesTableSeeder extends Seeder
{


    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::table('states')->delete();

        $this->migrateFromFile();

        State::create(
            [

                LocationEnums::ID         => 4964,
                LocationEnums::NAME       => 'Asunción',
                LocationEnums::COUNTRY_ID => 173,

            ]
        );

        $this->call(CitiesTableSeeder::class);
    } //end run()


    public function migrateFromFile($filePath = "database/seeders/states.json")
    {
        $string = file_get_contents($filePath);
        $states = json_decode($string, true);
        foreach ($states as $state) {
            State::create(
                [

                    LocationEnums::ID         => $state[LocationEnums::ID],
                    LocationEnums::NAME       => $state[LocationEnums::NAME],
                    LocationEnums::COUNTRY_ID => $state[LocationEnums::COUNTRY_ID],

                ]
            );
        } //end foreach
    } //end migrateFromFile()


}//end class
