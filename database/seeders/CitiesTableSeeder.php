<?php

namespace Database\Seeders;

use App\Enums\Locations\LocationEnums;
use App\Models\City;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CitiesTableSeeder extends Seeder
{


    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::table('cities')->delete();

        $this->migrateFromFile();

        City::create(
            [

                LocationEnums::NAME     => 'Asunción',
                LocationEnums::STATE_ID => 4964,

            ]
        );
    } //end run()


    public function migrateFromFile($filePath = "database/seeders/cities.json")
    {
        $string = file_get_contents($filePath);
        $cities = json_decode($string, true);
        foreach ($cities as $city) {
            City::create(
                [

                    LocationEnums::ID       => $city[LocationEnums::ID],
                    LocationEnums::NAME     => $city[LocationEnums::NAME],
                    LocationEnums::STATE_ID => $city[LocationEnums::STATE_ID],

                ]
            );
        } //end foreach
    } //end migrateFromFile()


}//end class
