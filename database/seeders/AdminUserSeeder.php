<?php

namespace Database\Seeders;

use App\Enums\Users\UserEnums;
use App\Models\User;
use Hash;
use Illuminate\Database\Seeder;

class AdminUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create(
            [
                UserEnums::NAME      => 'Jhonny Rafael',
                UserEnums::EMAIL      => 'admin@admin.com',
                UserEnums::PASSWORD      =>  Hash::make('123456789'),
                UserEnums::PHONE  => '+58-04249215484',
                UserEnums::USERID  => 1,
                UserEnums::DOCUMENT => '24.685.098',
                UserEnums::BIRTHDAY  => '1995-05-05',
                UserEnums::CITYID  => 4840,
                UserEnums::ISADMIN  => 1,
            ]
        );
    }
}
