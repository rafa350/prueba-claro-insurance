<?php

namespace Database\Seeders;

use App\Enums\Locations\LocationEnums;
use App\Models\Country;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CountriesTableSeeder extends Seeder
{


    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::table('countries')->delete();

        $this->migrateFromFile();

        $this->call(StatesTableSeeder::class);
    } //end run()


    public function migrateFromFile($filePath = "database/seeders/countries.json")
    {
        $string = file_get_contents($filePath);
        $countries = json_decode($string, true);
        foreach ($countries as  $country) {
            Country::create(
                [

                    LocationEnums::ID        => $country[LocationEnums::ID],
                    LocationEnums::NAME      => $country[LocationEnums::NAME],
                    LocationEnums::CODE      => $country[LocationEnums::CODE],
                    LocationEnums::FLAG      => $country[LocationEnums::FLAG],
                    LocationEnums::LATITUDE  => $country[LocationEnums::LATITUDE],
                    LocationEnums::LONGITUDE => $country[LocationEnums::LONGITUDE],
                    LocationEnums::CURRENCY  => $country[LocationEnums::CURRENCY],
                ]
            );
        } //end foreach
    } //end migrateFromFile()


}//end class
