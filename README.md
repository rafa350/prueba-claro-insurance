Prueba Para puesto de Fullstack

Instrucciones

-   composer install

-   cp .env.example .env

-   configurar base de datos en .env

-   ejecutar php artisan migrate --seed

-   ejecutar php artisan serv

-   usuario administrador: admin@admin.com , password: 123456789

-   ejecutar php artisan queue:work para el envio de correos
