<?php

use App\Http\Controllers\Api\Auth\AuthController;
use App\Http\Controllers\Api\Emails\EmailsController;
use App\Http\Controllers\Api\Locations\LocationsController;
use App\Http\Controllers\Api\Users\UsersController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('register', [AuthController::class, 'register']);
Route::post('login', [AuthController::class, 'login']);

Route::middleware('auth:api')->group(function () {
    Route::resource('users', UsersController::class);

    //locations
    Route::get('get_countries', [LocationsController::class, 'getCountries']);
    Route::get('get_states/{id}', [LocationsController::class, 'getStates']);
    Route::get('get_cities/{id}', [LocationsController::class, 'getCities']);
    Route::get('get_parents_from_city/{id}', [LocationsController::class, 'getParents']);

    //Emails
    Route::resource('emails', EmailsController::class);
});
