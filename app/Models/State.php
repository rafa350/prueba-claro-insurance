<?php

namespace App\Models;

use Illuminate\Database\Eloquent\{
    Factories\HasFactory,
    Model
};

class State extends Model
{
    use HasFactory;
    protected $table = 'states';

    protected $fillable = [
        'name',
        'country_id',
    ];
}
