<?php

namespace App\Models;

use Illuminate\Database\Eloquent\{
    Factories\HasFactory,
    Model
};

class Email extends Model
{
    use HasFactory;

    protected $table = 'emails';

    protected $fillable = [
        'subject',
        'destinatary',
        'content',
        'job_id',
        'user_id',
    ];
}
