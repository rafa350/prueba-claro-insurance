<?php

namespace App\Models;

use Illuminate\Database\Eloquent\{
    Factories\HasFactory,
    Model
};

class City extends Model
{
    use HasFactory;
    protected $table = 'cities';

    protected $fillable = [
        'name',
        'state_id',
    ];
}
