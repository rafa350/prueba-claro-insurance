<?php

namespace App\Services;

use App\Repositories\LocationRepository;

class LocationService
{
    /**
     * @var $LocationRepository
     */
    protected $LocationRepository;

    /**
     * PromotionService constructor.
     *
     * @param LocationRepository $LocationRepository
     */
    public function __construct(LocationRepository $LocationRepository)
    {
        $this->LocationRepository = $LocationRepository;
    }
    public function getLocationById($id)
    {
        return $this->LocationRepository->getLocationById($id);
    }

    public function getCountries()
    {
        return $this->LocationRepository->getCountries();
    }
    public function getStates($id)
    {
        return $this->LocationRepository->getStates($id);
    }
    public function getCities($id)
    {
        return $this->LocationRepository->getCities($id);
    }
    public function getParents($id)
    {
        return $this->LocationRepository->getParents($id);
    }
}
