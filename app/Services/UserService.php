<?php

namespace App\Services;

use App\Repositories\UserRepository;
use DB;
use Exception;

class UserService
{
    /**
     * @var $UserRepository
     */
    protected $UserRepository;

    /**
     * PromotionService constructor.
     *
     * @param UserRepository $UserRepository
     */
    public function __construct(UserRepository $UserRepository)
    {
        $this->UserRepository = $UserRepository;
    }
    public function getUserById($id)
    {
        return $this->UserRepository->getUserById($id);
    }

    public function getAll($data)
    {
        return $this->UserRepository->getAll($data);
    }
    public function store($data)
    {

        DB::beginTransaction();

        try {
            $user = $this->UserRepository->store($data);
        } catch (Exception $e) {
            DB::rollBack();
            throw $e;
        }

        DB::commit();

        return $user;
    }

    public function update($data)
    {

        DB::beginTransaction();

        try {
            $user = $this->UserRepository->update($data);
        } catch (Exception $e) {
            DB::rollBack();
            throw $e;
        }

        DB::commit();

        return $user;
    }

    public function destroy($id)
    {

        DB::beginTransaction();

        try {
            $user = $this->UserRepository->destroy($id);
        } catch (Exception $e) {
            DB::rollBack();
            throw $e;
        }

        DB::commit();

        return $user;
    }
}
