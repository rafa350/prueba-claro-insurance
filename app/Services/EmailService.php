<?php

namespace App\Services;

use App\Repositories\EmailRepository;
use DB;
use Exception;

class EmailService
{
    /**
     * @var $EmailRepository
     */
    protected $EmailRepository;

    /**
     * PromotionService constructor.
     *
     * @param EmailRepository $EmailRepository
     */
    public function __construct(EmailRepository $EmailRepository)
    {
        $this->EmailRepository = $EmailRepository;
    }
    public function getEmailById($id)
    {
        return $this->EmailRepository->getEmailById($id);
    }

    public function getAll($data)
    {
        return $this->EmailRepository->getAll($data);
    }
    public function store($data)
    {

        DB::beginTransaction();

        try {
            $user = $this->EmailRepository->store($data);
        } catch (Exception $e) {
            DB::rollBack();
            throw $e;
        }

        DB::commit();

        return $user;
    }

    public function update($data)
    {

        DB::beginTransaction();

        try {
            $user = $this->EmailRepository->update($data);
        } catch (Exception $e) {
            DB::rollBack();
            throw $e;
        }

        DB::commit();

        return $user;
    }

    public function destroy($id)
    {

        DB::beginTransaction();

        try {
            $user = $this->EmailRepository->destroy($id);
        } catch (Exception $e) {
            DB::rollBack();
            throw $e;
        }

        DB::commit();

        return $user;
    }
}
