<?php

namespace App\Http\Controllers\Api\Users;

use App\Http\Controllers\Controller;
use App\Http\Requests\Users\StoreUserRequest;
use App\Services\UserService;
use App\Traits\ApiResponser;
use Illuminate\Http\Request;

class UsersController extends Controller
{
    protected $UserService;
    use ApiResponser;

    /**
     * Create the controller instance.
     *
     * @return void
     */
    public function __construct(UserService $UserService)
    {
        $this->UserService = $UserService;
    }


    public function index(Request $request)
    {
        $data = $request->only(['descending', 'search',  'itemsPerPage', 'sortBy']);

        $retorno['data'] = $this->UserService->getAll($data);

        return $this->success(
            $retorno['data']
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreUserRequest $request)
    {
        $data = $request->only(['name', 'email', 'phone', 'birthday', 'password', 'document', 'city_id', 'user_id']);

        $retorno['data'] = $this->UserService->store($data);

        return $this->success(
            $retorno['data']
        );
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\User  $User
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->only(['name',  'phone', 'birthday',  'city_id', 'user_id']);
        $data['id'] = $id;

        $retorno['data'] = $this->UserService->update($data);

        return $this->success(
            $retorno['data']
        );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\User  $User
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $retorno['data'] = $this->UserService->destroy($id);

        return $this->success(
            $retorno['data']
        );
    }
}
