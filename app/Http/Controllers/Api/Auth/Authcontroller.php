<?php

namespace App\Http\Controllers\Api\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\LoginRequest;
use App\Services\UserService;
use App\Traits\ApiResponser;
use Illuminate\Support\Facades\Auth;

class Authcontroller extends Controller
{
    use ApiResponser;
    protected $UserService;

    public function __construct(UserService $UserService)
    {
        $this->UserService = $UserService;
    }
    /**
     * Login api
     *
     * @return \Illuminate\Http\Response
     */
    public function login(LoginRequest $request)
    {
        if (!Auth::attempt($request->only('email', 'password'))) {
            return $this->error('Datos Incorretos, verifiquelos e intente nuevamente.', 401);
        }

        $user = $this->UserService->getUserById(auth()->user()->id);

        return $this->success([
            'token' => $user->createToken('API Token')->plainTextToken,
            'user' =>  $user,
        ]);
    }
}
