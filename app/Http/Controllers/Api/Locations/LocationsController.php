<?php

namespace App\Http\Controllers\Api\Locations;

use App\Http\Controllers\Controller;
use App\Services\LocationService;
use App\Traits\ApiResponser;

class LocationsController extends Controller
{
    protected $LocationService;
    use ApiResponser;

    /**
     * Create the controller instance.
     *
     * @return void
     */
    public function __construct(LocationService $LocationService)
    {
        $this->LocationService = $LocationService;
    }


    public function getCountries()
    {
        $retorno['data'] = $this->LocationService->getCountries();

        return $this->success(
            $retorno['data']
        );
    }

    public function getStates($id)
    {
        $retorno['data'] = $this->LocationService->getStates($id);

        return $this->success(
            $retorno['data']
        );
    }

    public function getCities($id)
    {
        $retorno['data'] = $this->LocationService->getCities($id);

        return $this->success(
            $retorno['data']
        );
    }

    public function getParents($id)
    {
        $retorno['data'] = $this->LocationService->getParents($id);

        return $this->success(
            $retorno['data']
        );
    }
}
