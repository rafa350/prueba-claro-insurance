<?php

namespace App\Http\Controllers\Api\Emails;

use App\Http\Controllers\Controller;
use App\Services\EmailService;
use App\Traits\ApiResponser;
use Auth;
use Illuminate\Http\Request;

class EmailsController extends Controller
{
    protected $EmailService;
    use ApiResponser;

    /**
     * Create the controller instance.
     *
     * @return void
     */
    public function __construct(EmailService $EmailService)
    {
        $this->EmailService = $EmailService;
    }


    public function index(Request $request)
    {
        $data = $request->only(['descending', 'search',  'itemsPerPage', 'sortBy']);
        $data['user_id'] = Auth::user()->id;
        $retorno['data'] = $this->EmailService->getAll($data);
        return $this->success(
            $retorno['data']
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->only(['subject', 'destinatary', 'content']);
        $data['user_id'] = Auth::user()->id;
        $retorno['data'] = $this->EmailService->store($data);

        return $this->success(
            $retorno['data']
        );
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Email  $Email
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->only(['name',  'phone', 'birthday',  'city_id', 'user_id']);
        $data['id'] = $id;

        $retorno['data'] = $this->EmailService->update($data);

        return $this->success(
            $retorno['data']
        );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Email  $Email
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $retorno['data'] = $this->EmailService->destroy($id);

        return $this->success(
            $retorno['data']
        );
    }
}
