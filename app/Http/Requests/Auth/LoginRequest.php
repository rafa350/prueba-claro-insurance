<?php

namespace App\Http\Requests\Auth;

use App\Enums\Validations\ValidateContansEnum;
use Illuminate\Foundation\Http\FormRequest;

class LoginRequest extends FormRequest implements ValidateContansEnum

{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    protected function prepareForValidation()
    {
        $this->id = $this->route('client');
        $this->merge(['id'        => (int) $this->id]);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => 'required|string|email|',
            'password' => 'required|string|min:6'
        ];
    }

    public function messages()
    {
        return self::MESSAGES;
    }

    public function attributes()
    {
        return self::ATTRIBUTES;
    }
}
