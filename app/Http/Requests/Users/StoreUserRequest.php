<?php

namespace App\Http\Requests\Users;

use App\Enums\Validations\ValidateContansEnum;
use Illuminate\Foundation\Http\FormRequest;

class StoreUserRequest extends FormRequest implements ValidateContansEnum
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    protected function prepareForValidation()
    {
        $this->id = $this->route('client');
        $this->merge(['id'        => (int) $this->id]);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => 'required|string|email|unique:users,email',
            'password' => 'required|string|min:8',
            'name' => 'required|string|max:100',
            'birthday' => 'required',
            'document' => 'required|string|max:11',
            'city_id' => 'required',
            'user_id' => 'required',
        ];
    }

    public function messages()
    {
        return self::MESSAGES;
    }

    public function attributes()
    {
        return self::ATTRIBUTES;
    }
}
