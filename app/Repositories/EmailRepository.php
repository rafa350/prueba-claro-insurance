<?php

namespace App\Repositories;

use App\Mail\ClaroEmail;
use App\Models\Email;
use DB;
use Mail;

class EmailRepository
{

    protected $Auth;

    public function __construct(Email $email)
    {
        $this->email = $email;
    }

    public function getEmailById($id)
    {
        return  $this->email::find($id);
    }

    public function getAll($data)
    {
        $search = $data['search'] ?? '';
        $sortBy = $data['sortBy'] ?? '';
        $descending = $data['descending'] ?? '';
        $emails =  $this->email
            ->whereRaw("(CONCAT(emails.subject,' ',emails.content,' ',emails.destinatary) like '%$search%')")
            ->when($data['user_id'] !== 1, function ($q) use ($data) {
                return $q->where('user_id', $data['user_id']);
            })
            ->orderBy($sortBy, $descending)
            ->paginate($data['itemsPerPage']);
        //verificamos si el job asociado aun sigue alli
        foreach ($emails as  $email) {
            $job = DB::table('jobs')->where('id', $email->job_id)->first();
            if (empty($job)) {
                $email->status = 'sent';
                $email->save();
            }
        }
        // return $data['user_id'];
        return $emails;
    }
    public function store($data)
    {

        Mail::to($data['destinatary'])->queue(new ClaroEmail($data['subject'], $data['content']));
        $job = DB::table('jobs')->orderBy('id', 'DESC')->first();

        $email =  $this->email::create($data);

        $email->job_id = $job->id;
        $email->save();
        return $email;
    }
}
