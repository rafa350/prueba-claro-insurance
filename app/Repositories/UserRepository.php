<?php

namespace App\Repositories;

use App\Enums\Users\UserEnums;
use App\Models\User;
use Hash;

class UserRepository
{

    protected $Auth;

    public function __construct(User $user)
    {
        $this->user = $user;
    }

    public function getUserById($id)
    {
        return  $this->user::find($id);
    }

    public function getAll($data)
    {
        $search = $data['search'] ?? '';
        $sortBy = $data['sortBy'] ?? '';
        $descending = $data['descending'] ?? '';
        return $this->user
            ->whereRaw("(CONCAT(users.name,' ',users.email,' ',users.phone,' ',users.email,' ',users.document) like '%$search%')")
            ->orderBy($sortBy, $descending)
            ->paginate($data['itemsPerPage']);
    }
    public function store($data)
    {
        return  $this->user::create(
            [
                UserEnums::NAME      => $data['name'],
                UserEnums::EMAIL      => $data['email'],
                UserEnums::PASSWORD      =>  Hash::make($data['password']),
                UserEnums::PHONE  => $data['phone'],
                UserEnums::USERID  => $data['user_id'],
                UserEnums::DOCUMENT => $data['document'],
                UserEnums::BIRTHDAY  => $data['birthday'],
                UserEnums::CITYID  => $data['city_id'],
            ]
        );
    }

    public function update($data)
    {
        return  $this->user::find($data['id'])->update(
            [
                UserEnums::NAME      => $data['name'],
                UserEnums::PHONE  => $data['phone'],
                UserEnums::USERID  => $data['user_id'],
                UserEnums::BIRTHDAY  => $data['birthday'],
                UserEnums::CITYID  => $data['city_id']
            ]
        );
    }

    public function destroy($id)
    {

        $user =  $this->user::find($id);
        $user->delete();
        return $user;
    }
}
