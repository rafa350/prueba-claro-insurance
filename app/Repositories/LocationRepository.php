<?php

namespace App\Repositories;

use App\Models\{
    City,
    Country,
    State
};

class LocationRepository
{

    protected $Auth;

    public function __construct(Country $country, State $state, City $city)
    {
        $this->country = $country;
        $this->state = $state;
        $this->city = $city;
    }

    public function getCountries()
    {
        return  $this->country::select('id', 'name')->get();
    }


    public function getStates($id)
    {
        return  $this->state::where('country_id', $id)->select('id', 'name')->get();
    }

    public function getCities($id)
    {
        return  $this->city::where('state_id', $id)->select('id', 'name')->get();
    }

    public function getParents($id)
    {
        $city =  $this->city::find($id);
        $state =  $this->state::find($city->state_id);
        $country =  $this->city::find($state->country_id);

        $parents = new \stdClass();
        $parents->state = $state->id;
        $parents->country = $country->id;
        $parents->city = $city->id;

        return $parents;
    }
}
