<?php

namespace App\Traits;

use App\Mail\Items\ReceiptMail;
use App\Mail\Users\CompleteRegister;
use Mail;

/*
|--------------------------------------------------------------------------
| GlobalFunctions Trait
|--------------------------------------------------------------------------
|
| trait de funciones utiles
|
*/

trait MailSender
{
    protected function sendTicketToUserEmail($userEmail, $ticket)
    {
        try {
            Mail::to($userEmail)->send(new ReceiptMail('Comprobante de Compra', $ticket));
        } catch (\Exception $e) {
            \Log::debug($e);

            return $e;
        }
    }

    protected function sendCompleteRegisterUserEmail($userEmail, $user)
    {
        try {
            Mail::to($userEmail)->send(new CompleteRegister('Confirmacion de Correo', $user));
        } catch (\Exception $e) {
            \Log::debug($e);
            return $e;
        }
    }
}
