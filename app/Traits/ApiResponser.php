<?php

namespace App\Traits;

use Carbon\Carbon;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Validator as Validator2;

/*
|--------------------------------------------------------------------------
| Api Responser Trait
|--------------------------------------------------------------------------
|
| This trait will be used for any response we sent to clients.
|
*/

trait ApiResponser
{
    /**
     * Return a success JSON response.
     *
     * @param  array|string  $data
     * @param  string  $message
     * @param  int|null  $code
     * @return \Illuminate\Http\JsonResponse
     */
    protected function success($data, string $message = null, int $code = 200)
    {
        return response()->json([
            'success' => true,
            'message' => $message,
            'data' => $data
        ], $code);
    }

    /**
     * Return an error JSON response.
     *
     * @param  string  $message
     * @param  int  $code
     * @param  array|string|null  $data
     * @return \Illuminate\Http\JsonResponse
     */
    protected function error(string $message = null, int $code, $data = null)
    {
        return response()->json([
            'success' => false,
            'message' => $message,
            'data' => $data
        ], $code);
    }

    /**
     * Return an error JSON response.
     *
     * @param  string  $message
     * @param  int  $code
     * @param  array|string|null  $data
     * @return \Illuminate\Http\JsonResponse
     */
    protected function errorValidation(?Validator2 $validator, ?array $rules)
    {
        $salida  = false;
        if (!$validator) {
            $validator = Validator::make(request()->all(), $rules);
        }

        if ($validator->fails()) {
            $salida =  response()->json([
                'success' => false,
                'message' => 'Error validación',
                'data' => $validator->errors()->all()
            ], 422);
        }
        return $salida;
    }

    protected function errorValidation2(array $rules)
    {
        $salida  = false;
        $validator = Validator::make(request()->all(), $rules);

        if ($validator->fails()) {
            $salida =  response()->json([
                'success' => false,
                'message' => 'Error validación',
                'data' => $validator->errors()->all()
            ], 422);
        }
        return $salida;
    }
}
