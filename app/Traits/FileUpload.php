<?php

namespace App\Traits;

use Storage;
use Illuminate\Support\Str;
use File;

/*
|--------------------------------------------------------------------------
| Api Responser Trait
|--------------------------------------------------------------------------
|
| This trait will be used for any response we sent to clients.
|
*/

trait FileUpload
{
    /**
     * upload a file.
     *
     * @param  array|string  $data
     * @param  string  $message
     * @param  int|null  $code
     * @return \Illuminate\Http\JsonResponse
     */
    protected function uploadFile($file)
    {

        $fileName = Str::random(32);
        $fileName = $fileName . '.' . $file->getClientOriginalExtension();

        Storage::disk('temp-file')->put($fileName, File::get($file));

        return $fileName;
    }
}
