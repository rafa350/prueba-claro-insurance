<?php

namespace App\Traits;

use App\Services\ParameterService;

use App\Models\{
    Client,
    Parameter
};

/*
|--------------------------------------------------------------------------
| GlobalFunctions Trait
|--------------------------------------------------------------------------
|
| trait de funciones utiles
|
*/

trait GlobalFunctions
{
    public function __construct(ParameterService $ParameterService)
    {
        $this->ParameterService = $ParameterService;
    }
    /**
     * Return a success JSON response.
     *
     * @param  array|string  $data
     * @param  string  $message
     * @param  int|null  $code
     * @return \Illuminate\Http\JsonResponse
     */
    protected function addPoints($clientId, $total)
    {
        $points = Parameter::where('name', 'Puntos')->first();
        if (!empty($points)) {
            $OrganizationsPoints = $points->organizationParameters()->first(); // buscamos el parametros de puntos en la organizacion

            $client = Client::find($clientId); // buscamos el cliente

            if ($OrganizationsPoints) {
                if ($client->points) {
                    $client->points = $client->points + ($total * $OrganizationsPoints->value) / 100; //porque es porcentaje
                } else {
                    $client->points = ($total * $OrganizationsPoints->value) / 100; //porque es porcentaje
                }
                $client->save();
            }
        }
    }
}
