<?php

namespace App\Enums\Validations;

interface ValidateContansEnum
{
    const  ATTRIBUTES = [
        "article"              => "Referencia",
        "salePrice"              => "Precio de Venta",
        "primeCost"              => "Precio de Costo",
        "barcode"              => "Código de Barras",
        "type"              => "Tipo",
        "item_name"              => "Nombre",
        "representation" => "Representación en el TPV",
    ];

    const  MESSAGES = [
        "string"        => "El campo :attribute debe tener caracteres validos.",
        "unique"        => "El campo :attribute ya ha sido registrado. Por favor intente con otro.",
        "required"      => "El campo :attribute es requerido.",
        "integer"       => "El campo :attribute debe ser un número.",
    ];
}//end interface
