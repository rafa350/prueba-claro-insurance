<?php

namespace App\Enums\Users;

interface UserEnums
{
    const ID = 'id';
    const NAME = 'name';
    const EMAIL = 'email';
    const PASSWORD = 'password';
    const PHONE = 'phone';
    const DOCUMENT = 'document';
    const BIRTHDAY = 'birthday';
    const CITYID = 'city_id';
    const USERID = 'user_id';
    const ISADMIN = 'is_admin';
}
