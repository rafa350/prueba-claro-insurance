<?php

namespace App\Enums\Locations;

interface LocationEnums
{
    const ID = 'id';
    const NAME = 'name';
    const CODE = 'code';
    const FLAG = 'flag';
    const LATITUDE = 'latitude';
    const LONGITUDE = 'longitude';
    const CURRENCY = 'currency';
    const COUNTRY_ID = 'country_id';
    const STATE_ID = 'state_id';
}
